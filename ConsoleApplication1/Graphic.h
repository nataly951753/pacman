#pragma once
#include<windows.h>//setColor();setCursor()
#include "GlobalVariabel.h"//���������� ����������
using namespace std;

HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);/*�������� ���������� ��������� ����*/
void setColor(Color text, Color background)
{
	SetConsoleTextAttribute(hStdOut, (WORD)((background << 4) | text));
}

void setCursor(int x, int y)
{
	COORD c = { x, y };
	SetConsoleCursorPosition(/*�������� ���������� ��������� ����*/hStdOut, c);
}
void ShowPacman(int kx, int ky)//��������� �������
{
	setCursor(kx, ky);
	setColor(Yellow, Black);
	cout << pacman;
}
void ShowGhosts(int kx1, int ky1, int color)//��������� ����������
{
	setCursor(kx1, ky1);
	setColor(Color(color), Black);
	cout << ghost;
}
void CllearPacman(int kx, int ky)//������������� �������
{
	setCursor(kx, ky);
	setColor(Black, Black);
	cout << pacman;
}
void CllearGhost(char mas[][43], int kx1, int ky1)//��������� ����������
{
	setCursor(kx1, ky1);
	if (mas[ky1][kx1] == ' ')setColor(Black, Black);
	if (mas[ky1][kx1] == '-')setColor(Blue, Blue);
	if (mas[ky1][kx1] == '.')setColor(Yellow, Black);
	if (mas[ky1][kx1] == '*')setColor(LightBlue, Black);
	cout << mas[ky1][kx1];

}
void DrawLabirint(char mas[][43], int width, int height)//��������� ���������
{
	system("cls");
	for (size_t i = 0; i < height; i++)
	{
		for (size_t j = 0; j < width; j++)
		{
			if (mas[i][j] == '|')//�����
			{
				setColor(Magenta, LightGray);
				cout << " ";
			}
			else if (mas[i][j] == ' ')//�������
			{
				setColor(Magenta, Black);
				cout << " ";
			}
			else if (mas[i][j] == '-')//�����
			{
				setColor(Magenta, Blue);
				cout << " ";
			}
			else if (mas[i][j] == '1' || mas[i][j] == '2' || mas[i][j] == '3')//����
			{
				mas[i][j] = '.';
				setColor(Yellow, Black);
				cout << mas[i][j];
			}
			else if (mas[i][j] == '*')//��������
			{
				setColor(White, Black);
				cout << "*";
			}
			else if (mas[i][j] == '.')//����
			{
				setColor(Yellow, Black);
				cout << mas[i][j];
			}
		}
		cout << "\n";
	}
	setCursor(0, 40);
	setColor(LightBlue, Black);
	cout << "    ��� ����� ������� Enter ��� Space ";

}
void Pravila()
{
	system("cls");

	setCursor(4, 2);
	setColor(Yellow, Black);
	cout << "C - ������";

	setCursor(20, 2);
	setColor(LightRed, Black);
	cout << "A";
	setCursor(22, 2);
	setColor(LightBlue, Black);
	cout << "A";
	setCursor(24, 2);
	setColor(LightGreen, Black);
	cout << "A";
	setCursor(26, 2);
	setColor(LightMagenta, Black);
	cout << "A";
	setCursor(28, 2);
	setColor(LightCyan, Black);
	cout << "- ����������";
	setCursor(15, 4);
	setColor(White, Black);
	cout << "* - ��������";
	setCursor(4, 8);
	setColor(LightGray, Black);
	cout << "����:  �������(������) ��� ������ �����(���), ������� ������������ � ������������.";

	setCursor(4, 12);
	setColor(LightGreen, Black);
	cout << "������������ �������������� � �������  ������� �� ����������.";

	setCursor(4, 16);
	setColor(LightCyan, Black);
	cout << "���� ������ ����� ��������, �� �� ���- ������ ������ ���������� ������ ���������  � ���� ����� �������� ������� � ��������.  ��������� ���������� ������������ � �����.";

	setCursor(4, 22);
	setColor(LightMagenta, Black);
	cout << "���� ����� � �������, ������� �������  ��� ������ �� ������� ������, �� ������    ������ � ��������������� �������.";
	setCursor(4, 31);
	setColor(Yellow, Black);
	cout << "��� ����� ������� Enter ��� Space. ";
	setCursor(4, 27);
	setColor(LightBlue, Black);
	cout << "���� ������ ��������� ����������� ���  ����, �� ���� ���������� ����������. ";
	setCursor(4, 38);
	setColor(White, Black);
	cout << "��� ����������� � ���� ������� �����                     ������� ";
	getch();
}
void FoodLife()
{
	setCursor(0, 39);
	setColor(White, Black);
	cout << "��� " << Food << "\t\t\t����� " << life;
	
}

void GameOver()
{
	system("cls");//�������� �������
	setColor(LightRed, LightRed);
	//����� "G"
	setCursor(6, 12); cout << "    ";
	setCursor(5, 13); cout << "  "; setCursor(10, 13); cout << " ";
	setCursor(4, 14); cout << "  ";
	setCursor(4, 15);	cout << "  ";  setCursor(9, 15); cout << "  ";
	setCursor(5, 16);	cout << "  "; setCursor(10, 16); cout << " ";
	setCursor(6, 17); cout << "    ";
	//����� "�"
	setCursor(16, 12); cout << " ";
	setCursor(15, 13); cout << "   ";
	setCursor(14, 14); cout << "     "; setCursor(14, 15); cout << "  ";
	setCursor(17, 15); cout << "  ";
	setCursor(13, 16); cout << "       ";
	setCursor(13, 17); cout << "  "; setCursor(18, 17); cout << "  ";
	//����� "M"
	setCursor(22, 12); cout << " "; setCursor(28, 12); cout << " ";
	setCursor(22, 13); cout << "  "; setCursor(27, 13); cout << "  ";
	setCursor(22, 14); cout << "   "; setCursor(26, 14); cout << "   ";
	setCursor(22, 15); cout << "       ";
	setCursor(22, 16); cout << "  "; setCursor(25, 16); cout << " "; setCursor(27, 16); cout << "  ";
	setCursor(22, 17); cout << "  "; setCursor(27, 17); cout << "  ";
	//����� "E"
	setCursor(31, 12); cout << "       ";
	setCursor(31, 13); cout << "  ";
	setCursor(31, 14); cout << "       ";
	setCursor(31, 15); cout << "  ";
	setCursor(31, 16); cout << "  ";
	setCursor(31, 17); cout << "       ";
	//����� "O"
	setCursor(5, 20); cout << "    ";
	setCursor(4, 21); cout << "  "; setCursor(8, 21); cout << "  ";
	setCursor(3, 22); cout << "  "; setCursor(9, 22); cout << "  ";
	setCursor(3, 23);	cout << "  ";   setCursor(9, 23); cout << "  ";
	setCursor(4, 24);	cout << "  "; setCursor(8, 24); cout << "  ";
	setCursor(5, 25); cout << "    ";
	//����� "V"
	setCursor(12, 20); cout << "  "; setCursor(19, 20); cout << "  ";
	setCursor(13, 21); cout << "  "; setCursor(18, 21); cout << "  ";
	setCursor(13, 22); cout << "  "; setCursor(18, 22); cout << "  ";
	setCursor(14, 23); cout << "  "; setCursor(17, 23); cout << "  ";
	setCursor(14, 24); cout << "  "; setCursor(17, 24); cout << "  ";
	setCursor(15, 25); cout << "   ";
	//����� "E"
	setCursor(22, 20); cout << "       ";
	setCursor(22, 21); cout << "  ";
	setCursor(22, 22); cout << "       ";
	setCursor(22, 23); cout << "  ";
	setCursor(22, 24); cout << "  ";
	setCursor(22, 25); cout << "       ";
	//����� "R"
	setCursor(31, 20); cout << "      ";
	setCursor(31, 21); cout << "  "; setCursor(36, 21); cout << "  ";
	setCursor(31, 22); cout << "  "; setCursor(36, 22); cout << "  ";
	setCursor(31, 23); cout << "      ";
	setCursor(31, 24); cout << "  "; setCursor(34, 24); cout << "  ";
	setCursor(31, 25); cout << "  "; setCursor(35, 25); cout << "  ";
	setColor(LightRed, Black);
	setCursor(15, 28); cout << "������� " << Food;
	setColor(Black, Black);
	_getch();//�������� ������� �������
}

void Win()//����������� ����� ��������� � ������
{
	system("cls");//�������� �������
	setColor(Yellow, Yellow);
	//����� "W"
	setCursor(2, 13); cout << "  "; setCursor(13, 13); cout << "  ";
	setCursor(3, 14); cout << "  "; setCursor(12, 14); cout << "  ";
	setCursor(3, 15); cout << "  "; setCursor(12, 15); cout << "  ";
	setCursor(4, 16); cout << "  ";  setCursor(8, 16); cout << " "; setCursor(11, 16); cout << "  ";
	setCursor(4, 17); cout << "  "; setCursor(7, 17); cout << "  "; setCursor(9, 17); cout << " "; setCursor(11, 17); cout << "  ";
	setCursor(5, 18); cout << "  "; setCursor(10, 18); cout << "  ";
	//����� "I"
	setCursor(19, 13); cout << "        ";
	setCursor(22, 14); cout << "  ";
	setCursor(22, 15); cout << "  ";
	setCursor(22, 16); cout << "  ";
	setCursor(22, 17); cout << "  ";
	setCursor(19, 18); cout << "        ";
	//����� "N"
	setCursor(32, 13); cout << "  "; setCursor(38, 13); cout << "  ";
	setCursor(32, 14); cout << "   "; setCursor(38, 14); cout << "  ";
	setCursor(32, 15); cout << "    "; setCursor(38, 15); cout << "  ";
	setCursor(32, 16); cout << "     "; setCursor(38, 16); cout << "  ";
	setCursor(32, 17); cout << "  "; setCursor(35, 17); cout << "     ";
	setCursor(32, 18); cout << "  "; setCursor(36, 18); cout << "    ";
	setColor(Yellow, Black);
	setCursor(17, 21); cout << "������� " << Food;
	

	int xp = 11, yp = 30, yg1 = 30, yg2 = 30, yg3 = 30, yg4 = 30, xg1 = 6, xg2 = 4, xg3 = 2, xg4 = 0;//���������� �������� ���������� � �������
	int move = 0, fff = 0;
	do
	{
		move++;//������� ��������
		if (move % 3000 == 0)//������������� ������������ �� 3000 ��������
		{
			move = 0;//��������� �������� ��������
			CllearPacman(xp, yp);
			xp++;//����������� �������
			if (xp > 42)//���� ��������� ����� ������
			{
				xp = 0;//��������� � ������
			}
			ShowPacman(xp, yp);
			CllearPacman(xg1, yg1);
			xg1++;//����������� ��������
			if (xg1 > 42)//���� ��������� ����� ������
			{
				xg1 = 0;//��������� � ������
			}
			ShowGhosts(xg1, yg1, LightRed);
			CllearPacman(xg2, yg2);
			xg2++;//����������� ��������
			if (xg2 > 42)//���� ��������� ����� ������
			{
				xg2 = 0;//��������� � ������
			}
			ShowGhosts(xg2, yg2, LightBlue);
			CllearPacman(xg3, yg3);
			xg3++;//����������� ��������
			if (xg3 > 42)//���� ��������� ����� ������
			{
				xg3 = 0;//��������� � ������
			}
			ShowGhosts(xg3, yg3, LightGreen);
			CllearPacman(xg4, yg4);
			xg4++;//����������� ��������
			if (xg4 > 42)//���� ��������� ����� ������
			{
				xg4 = 0;//��������� � ������
			}
			ShowGhosts(xg4, yg4, LightMagenta);
		}
		switch (_kbhit())//�������� ������ �� �������
		{
		case 1:
		{
			fff = 1; //���� ������� ������, �������� ������������
		}break;
		}
	} while (!fff);
	setColor(Black, Black);

	
}
void MenuGraphic()//����������� ����� ����
{
	system("cls");//������� �������
	vubor = 0;
	setCursor(0, 5);
	setColor(Yellow, Yellow);
	//����� ����� "P"
	cout << "     "; setCursor(0, 6);
	cout << "  "; setCursor(4, 6);	cout << "  "; setCursor(0, 7);
	cout << "  "; setCursor(4, 7); cout << "  "; setCursor(0, 8);
	cout << "     "; setCursor(0, 9);
	cout << "   "; setCursor(0, 10);
	cout << "   "; setCursor(9, 5);
	//����� ����� "A"
	cout << " "; setCursor(8, 6);
	cout << "   "; setCursor(7, 7);
	cout << "     "; setCursor(7, 8); cout << "  "; setCursor(10, 8);
	cout << "  "; setCursor(6, 9);
	cout << "       "; setCursor(6, 10);
	cout << "  "; setCursor(11, 10); cout << "  "; setCursor(15, 5);
	//����� ����� "C"
	cout << "   "; setCursor(14, 6);
	cout << "  "; setCursor(18, 6); cout << " "; setCursor(13, 7);
	cout << "  "; setCursor(13, 8);
	cout << "  "; setCursor(14, 9);
	cout << "  "; setCursor(18, 9); cout << " "; setCursor(15, 10);
	cout << "   "; setCursor(20, 5);
	//����� ����� "M"
	cout << " "; setCursor(26, 5); cout << " "; setCursor(20, 6);
	cout << "  "; setCursor(25, 6); cout << "  "; setCursor(20, 7);
	cout << "   "; setCursor(24, 7); cout << "   "; setCursor(20, 8);
	cout << "       "; setCursor(20, 9);
	cout << "  "; setCursor(23, 9); cout << " "; setCursor(25, 9); cout << "  "; setCursor(20, 10);
	cout << "  "; setCursor(25, 10); cout << "  "; setCursor(31, 5);
	//����� ����� "A"
	cout << " "; setCursor(30, 6);
	cout << "   "; setCursor(29, 7);
	cout << "     "; setCursor(29, 8); cout << "  "; setCursor(32, 8);
	cout << "  "; setCursor(28, 9);
	cout << "       "; setCursor(28, 10);
	cout << "  "; setCursor(33, 10); cout << "  "; setCursor(36, 5);
	//����� ����� "N"
	cout << " "; setCursor(41, 5); cout << "  "; setCursor(36, 6);
	cout << "  "; setCursor(41, 6); cout << "  "; setCursor(36, 7);
	cout << "   "; setCursor(41, 7); cout << "  "; setCursor(36, 8);
	cout << "    "; setCursor(41, 8); cout << "  "; setCursor(36, 9);
	cout << "       "; setCursor(36, 10);
	cout << "  "; setCursor(39, 10); cout << "    ";

	setCursor(19, 20); setColor(Yellow, Black);
	cout << "������";
	setCursor(15, 22); setColor(Yellow, Black);
	cout << "��������� ����";
	setCursor(19, 24); setColor(Yellow, Black);
	cout << "�������";
	setCursor(19, 26); setColor(Yellow, Black);
	cout << "�����";
}
void PauseGraphic()//����������� ����� ����
{
	system("cls");
	setColor(Yellow, Yellow);
	//����� ����� "�"
	setCursor(0, 5); cout << "        ";
	setCursor(0, 6); cout << "  "; setCursor(6, 6); cout << "  ";
	setCursor(0, 7); cout << "  "; setCursor(6, 7); cout << "  ";
	setCursor(0, 8); cout << "  "; setCursor(6, 8); cout << "  ";
	setCursor(0, 9); cout << "  "; setCursor(6, 9); cout << "  ";
	setCursor(0, 10); cout << "  "; setCursor(6, 10); cout << "  ";
	//����� ����� "�"
	setCursor(12, 5); cout << " "; setCursor(11, 6);
	cout << "   "; setCursor(10, 7);
	cout << "     "; setCursor(10, 8); cout << "  "; setCursor(13, 8);
	cout << "  "; setCursor(9, 9);
	cout << "       "; setCursor(9, 10);
	cout << "  "; setCursor(14, 10); cout << "  ";
	//����� ����� "�"
	setCursor(17, 5); cout << "  "; setCursor(24, 5); cout << "  ";
	setCursor(18, 6); cout << "  "; setCursor(23, 6); cout << "  ";
	setCursor(19, 7); cout << "  "; setCursor(22, 7); cout << "  ";
	setCursor(21, 8); cout << "  ";
	setCursor(20, 9); cout << "  ";
	setCursor(19, 10); cout << "  ";
	//����� ����� "�"
	setCursor(28, 5); cout << "       ";
	setCursor(33, 6); cout << "  ";
	setCursor(28, 7); cout << "       ";
	setCursor(33, 8); cout << "  ";
	setCursor(33, 9); cout << "  ";
	setCursor(28, 10); cout << "       ";
	//����� ����� "�"
	setCursor(39, 5); cout << " "; setCursor(38, 6);
	cout << "   "; setCursor(37, 7);
	cout << "     "; setCursor(37, 8); cout << "  "; setCursor(40, 8);
	cout << "  "; setCursor(36, 9);
	cout << "       "; setCursor(36, 10);
	cout << "  "; setCursor(41, 10); cout << "  ";

	setCursor(17, 20); setColor(Yellow, Black);
	cout << "����������";
	setCursor(15, 22); setColor(Yellow, Black);
	cout << "��������� ����";
	setCursor(18, 24); setColor(Yellow, Black);
	cout << "�������";
	setCursor(19, 26); setColor(Yellow, Black);
	cout << "�����";
}